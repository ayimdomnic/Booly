package com.infret;

import com.infret.services.IndexService;
import com.infret.utils.JsonTransformer;

import java.net.URLDecoder;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

public class InvertedIndexResource {

    private static final String API_CONTEXT = "/api/v1";

    private final IndexService todoService;

    public InvertedIndexResource(IndexService todoService) {
        this.todoService = todoService;
        setupEndpoints();
    }

    private void setupEndpoints() {
        // TODO: Agregar endpoints para exponer el servicio de indice invertido
        get(API_CONTEXT + "/get_stopwords", "application/json", (request, response) -> {
            return todoService.getStopWords();
        }, new JsonTransformer());

        post(API_CONTEXT + "/add_stopword/:word", "application/json", (request, response) -> {
            try {
                return todoService.addStopWord(URLDecoder.decode(request.params(":word"), "UTF-8"));
            } catch (Exception e) {
            }
            return false;
        }, new JsonTransformer());

        post(API_CONTEXT + "/remove_stopword/:word", "application/json", (request, response) -> {
            return todoService.removeStopWord(request.params(":word"));
        }, new JsonTransformer());


        post(API_CONTEXT + "/evaluate/:expr", "application/json", (request, response) -> {
            String expr = "";
            try {
                expr = URLDecoder.decode( request.params(":expr"), "UTF-8" );

            } catch ( Exception e ) {
                System.out.println("Error: " + e);
            }
            return todoService.evaluaExp( expr );

            //return true;
        }, new JsonTransformer() );
    }
}
