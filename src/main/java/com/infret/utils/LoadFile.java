package com.infret.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by christian on 04/10/15.
 */
public class LoadFile {

    /**
     * Método principal que lee los archivos a indexar y remueve los tags html.
     * TODO: Falta cambiar para leer desde los recursos del programa sin forzar el directorio.
     * @param fileName Es la ruta del archivo a leer.
     * @return: String con las palabras contenidas en el archivo.
     */
    public String getFile(String fileName) {
        File archivo = new File(fileName);
        String linea = "", resultado = "";
        try {
            if (archivo.exists()) {
                BufferedReader Leer = new BufferedReader(new FileReader(archivo));
                while (Leer.ready() == true) {
                    linea = Leer.readLine();
                    linea = removeTags(linea);
                    resultado += linea;
                }
                Leer.close();
            } else {
                System.out.println("No se encontro el archivo");
            }
        } catch (Exception ex) {
            System.out.println("No se pudo leer el archivo" + ex.getLocalizedMessage());
        }
        return resultado;
    }

    public  String removeTags(String line){
        char[] array = new char[line.length()];
        String contenido = "";
        int arrayIndex = 0;
        boolean inside = false;

        if (line.isEmpty())
            return contenido;

        for (int i = 0; i < line.length(); i++){
            char c = line.charAt(i);
            if (c == '<'){
                inside = true;
                continue;
            }
            if (c == '>'){
                inside = false;
                continue;
            }
            if (!inside){
                array[arrayIndex] = c;
                arrayIndex++;
            }

        }
        contenido = new String(array,0,arrayIndex);
        return contenido;
    }

    public ArrayList<String> getFilesFromDir(String dir){
        ArrayList<String> tmp = new ArrayList<>();

        try {
            Files.walk(Paths.get(dir)).forEach(filePath ->{
                if (Files.isRegularFile(filePath)){
                    System.out.println(filePath);
                    tmp.add(filePath.toString());
                }
            });
        } catch (Exception e){ }
        return tmp;
    }

}
