var app = angular.module('todoapp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

var API_VER = '/api/v1';

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/list.html',
        controller: 'ExpressionCtrl'
    }).when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
    }).when('/stopwords', {
        templateUrl: 'views/stopwords_list.html',
        controller: 'StopWordsCtrl'
    }).when('/stopwords_new', {
        templateUrl: 'views/stopwords_new.html',
        controller: 'StopWordsCtrl'
    }).otherwise({
        redirectTo: '/'
    })
});

app.controller('ExpressionCtrl', function ($scope, $http, $location) {
    $scope.evaluate_expression = function() {
        if ( $scope.expression.trim() === '' ) {
            $scope.documents = [];
            return;
        }

        $http.post(API_VER + '/evaluate/' +  escape($scope.expression)).success(function(data) {
            $scope.documents = data;
        });
    };
});

app.controller('StopWordsCtrl', function ($scope, $http, $location) {
    $scope.todo = {
        done: false
    };

    $scope.api_ver = "/api/v1";
    $scope.stopwords = [];

    $scope.init = function() {
        $http.get( $scope.api_ver + '/get_stopwords' ).success(function(data){
            $scope.stopwords = data;
        });
    };

    $scope.save = function() {
        console.log('Se agrega stop word');

        $http.post( $scope.api_ver + '/add_stopword/' + $scope.new_word).success(function(data) {
            $location.path('/stopwords');
        });
    };

    $scope.removeStopWord = function(word) {
        $http.post( $scope.api_ver + '/remove_stopword/' + word ).success(function(data){
            $scope.stopwords.splice( $scope.stopwords.indexOf(word), 1 );
        });
    };

    $scope.createTodo = function () {
        console.log($scope.todo);
        /*
        $http.post('/api/v1/todos', $scope.todo).success(function (data) {
            $location.path('/');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
        */
    };

    $scope.init();
});